<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Warehouse;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product =  new Product();
        $product->name = $request->name;
        $product->code = $request->code;
        $product->stock = $request->stock;
        $product->id_warehouse = $request->id_warehouse;
        $product->description = $request->description;
        $product->state = $request->state;
        $product->save();

        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product =  Product::find($id);
        $product->name = $request->name;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->state = $request->state;
        $product->save();

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getWarehouses()
    {
        return Warehouse::where('state',1)->get();
    }

    function filter(Request $request){

        $product =  Product::whereNotNull('name');

        if(isset($request->all) && $request->all == 1){
            return $product->get();
        }

        if(isset($request->name)){
            $product = $product->where('name', 'like', '%' . $request->name . '%');
        }

        if(isset($request->active) && $request->active == 1){
            $product = $product->orWhere('state', '1');
        }

        if(isset($request->inactive) && $request->inactive == 1){
            $product = $product->orWhere('state', '2');
        }

        if(isset($request->pending) && $request->pending == 1){
            $product = $product->orWhere('state', '3');
        }

        $product  =  $product->get();
        return $product;
    }
}
